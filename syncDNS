#!/bin/bash

if [[ -v DEBUG ]]; then
  set -x
fi

function _GET_CHANGES() {
  echo "Getting Changes ..."
  RESULT=($(echo "${ACURITE[@]}" "${ACUPARSE[@]}" | tr ' ' '\n' | sort | uniq -u))
  OLD=($(echo "${ACUPARSE[@]}" "${RESULT[@]}" | tr ' ' '\n' | sort | uniq -d | uniq))
  NEW=($(echo "${ACURITE[@]}" "${RESULT[@]}" | tr ' ' '\n' | sort | uniq -d | uniq))
}

function _GET_CF_RECORDS() {
  echo "Getting Cloudflare Records ..."
  CLOUDFLARE_RECORDS=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" |
    jq '.result[] | select(.name == "'"$ACUPARSE_DNS_NAME"'")')
}

function _CHECK_ACUPARSE_AFTER_UPDATE() {
  echo "Waiting 30 Seconds ..."
  sleep 30
  echo "Checking DNS ..."
  ACUPARSE_MODIFIED=$(dig @"$CF_NAMESERVER" +short "$ACUPARSE_DNS_NAME" | sort)
}

function _SEND_NOTIFICATION() {
  echo "Sending Notifications ..."
  curl -s --user "api:$MAILGUN_API_KEY" \
    "https://api.mailgun.net/v3/$MAILGUN_DOMAIN/messages" \
    -F from='Acuparse DNS Pipeline <noreply@acuparse.com>' \
    -F to='Acuparse CI Notifications <ci_notify@acuparse.com>' \
    -F subject='Acuparse DNS Change Successful' \
    -F text='Successfully updated Acuparse atlasapi DNS records.'

  curl -X POST -H 'Content-type: application/json' --data '{"text":"Acuparse atlasapi DNS records updated."}' 'https://hooks.slack.com/services/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'
}

function _SUCCESS() {
  if [ "$1" ]; then
    _SEND_NOTIFICATION
    printf "DONE: Updated %s.\n" "$1"
    printf "### Before ###\n\n"
    printf "Acuparse:\n%s\n\n" "$ACUPARSE"
    printf "### After ###\n\n"
    printf "Acuparse:\n%s\n\n" "$ACUPARSE_MODIFIED"
  else
    echo "DONE: Records already in sync."
    printf "AcuRite:\n%s\n\n" "$ACURITE"
    printf "Acuparse:\n%s\n\n" "$ACUPARSE"
  fi
  exit 0
}

## Main Script

echo "Checking DNS Records ... "
ACURITE=$(dig @"$ACURITE_NAMESERVER" "$ACURITE_DNS_NAME" +short | sort)
ACUPARSE=$(dig @"$CF_NAMESERVER" "$ACUPARSE_DNS_NAME" +short | sort)

echo "Comparing Records ..."
COMPARE_RECORDS=($(echo "${ACURITE[@]}" "${ACUPARSE[@]}" | tr ' ' '\n' | sort | uniq -u))

### Records Already Synced
if [ ${#COMPARE_RECORDS[@]} -eq 0 ]; then
  _SUCCESS

### One Change
elif [ ${#COMPARE_RECORDS[@]} -eq 2 ]; then
  printf "ONE Change Detected\nUpdating ONE ...\n"
  _GET_CHANGES

  printf "OLD: %s NEW: %s\n" "${OLD[0]}" "${NEW[0]}"

  _GET_CF_RECORDS

  CLOUDFLARE_ID_0=$(echo "$CLOUDFLARE_RECORDS" | jq -r --arg OLD "${OLD[0]}" 'select(.content == $OLD)|.id')

  CLOUDFLARE_UPDATE_0=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records/$CLOUDFLARE_ID_0" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" \
    --data '{"type":"A","name":"'"$ACUPARSE_DNS_NAME"'","content":"'"${NEW[0]}"'","ttl":1,"proxied":false}')

  printf "%s\n" "$CLOUDFLARE_UPDATE_0"

  _CHECK_ACUPARSE_AFTER_UPDATE

  _SUCCESS "ONE"

### Two Changes
elif [ ${#COMPARE_RECORDS[@]} -eq 4 ]; then
  printf "TWO Changes Detected\nUpdating TWO ...\n"

  _GET_CHANGES

  printf "OLD: %s NEW: %s\n" "${OLD[0]}" "${NEW[0]}"
  printf "OLD: %s NEW: %s\n" "${OLD[1]}" "${NEW[1]}"

  _GET_CF_RECORDS

  CLOUDFLARE_ID_0=$(echo "$CLOUDFLARE_RECORDS" | jq -r --arg OLD "${OLD[0]}" 'select(.content == $OLD)|.id')
  CLOUDFLARE_ID_1=$(echo "$CLOUDFLARE_RECORDS" | jq -r --arg OLD "${OLD[1]}" 'select(.content == $OLD)|.id')

  CLOUDFLARE_UPDATE_0=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records/$CLOUDFLARE_ID_0" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" \
    --data '{"type":"A","name":"'"$ACUPARSE_DNS_NAME"'","content":"'"${NEW[0]}"'","ttl":1,"proxied":false}')

  CLOUDFLARE_UPDATE_1=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records/$CLOUDFLARE_ID_1" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" \
    --data '{"type":"A","name":"'"$ACUPARSE_DNS_NAME"'","content":"'"${NEW[1]}"'","ttl":1,"proxied":false}')

  printf "%s\n" "$CLOUDFLARE_UPDATE_0"
  printf "%s\n" "$CLOUDFLARE_UPDATE_1"

  _CHECK_ACUPARSE_AFTER_UPDATE

  _SUCCESS "TWO"

### Three Changes
elif [ ${#COMPARE_RECORDS[@]} -eq 6 ]; then
  printf "THREE Changes Detected\nUpdating THREE ...\n"

  _GET_CHANGES

  printf "OLD: %s NEW: %s\n" "${OLD[0]}" "${NEW[0]}"
  printf "OLD: %s NEW: %s\n" "${OLD[1]}" "${NEW[1]}"
  printf "OLD: %s NEW: %s\n" "${OLD[2]}" "${NEW[2]}"

  _GET_CF_RECORDS

  CLOUDFLARE_ID_0=$(echo "$CLOUDFLARE_RECORDS" | jq -r --arg OLD "${OLD[0]}" 'select(.content == $OLD)|.id')
  CLOUDFLARE_ID_1=$(echo "$CLOUDFLARE_RECORDS" | jq -r --arg OLD "${OLD[1]}" 'select(.content == $OLD)|.id')
  CLOUDFLARE_ID_2=$(echo "$CLOUDFLARE_RECORDS" | jq -r --arg OLD "${OLD[2]}" 'select(.content == $OLD)|.id')

  CLOUDFLARE_UPDATE_0=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records/$CLOUDFLARE_ID_0" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" \
    --data '{"type":"A","name":"'"$ACUPARSE_DNS_NAME"'","content":"'"${NEW[0]}"'","ttl":1,"proxied":false}')

  CLOUDFLARE_UPDATE_1=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records/$CLOUDFLARE_ID_1" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" \
    --data '{"type":"A","name":"'"$ACUPARSE_DNS_NAME"'","content":"'"${NEW[1]}"'","ttl":1,"proxied":false}')

  CLOUDFLARE_UPDATE_2=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$CF_ZONE/dns_records/$CLOUDFLARE_ID_2" \
    -H "Authorization: Bearer $CF_TOKEN" \
    -H "Content-Type:application/json" \
    --data '{"type":"A","name":"'"$ACUPARSE_DNS_NAME"'","content":"'"${NEW[2]}"'","ttl":1,"proxied":false}')

  printf "%s\n" "$CLOUDFLARE_UPDATE_0"
  printf "%s\n" "$CLOUDFLARE_UPDATE_1"
  printf "%s\n" "$CLOUDFLARE_UPDATE_2"

  _CHECK_ACUPARSE_AFTER_UPDATE

  _SUCCESS "THREE"

### Compare Failed
else
  echo "ERROR: Comparing Records Failed"
  exit 1

fi
